export {
  getGameState,
  setGameState,
  getTurn,
  setTurn,
  getWinner,
  setWinner,
  getIntellect,
  setIntellect,
  getCells,
  getCell,
  setCell,
  getSums,
  getWinCells,
  setWinCells,
} from './core';

export { gameStart, setOnGameStopCallback, gameStop, setOnTurnCallback, onTurnComplete, turnByIntelect } from './logic';
