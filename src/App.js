import React, { Component } from 'react';
import './App.css';
import Game from './containers/Game/Game';

class App extends Component {
  render() {
    return (
      <div className="app">
        <header className="app-header">
          <h1>Tic Tac Toe game on React JS</h1>
        </header>

        <Game />

        <footer className="app-footer">
        </footer>
      </div>
    );
  }
}

export default App;
